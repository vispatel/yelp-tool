# encoding: utf-8

require 'rubygems'
require 'bundler'

begin
  Bundler.setup(:default, :development)
rescue Bundler::BundlerError => e
  $stderr.puts e.message
  $stderr.puts "Run `bundle install` to install missing gems"
  exit e.status_code
end
require 'rake'

require 'jeweler'
Jeweler::Tasks.new do |gem|
  # gem is a Gem::Specification... see http://docs.rubygems.org/read/chapter/20 for more options
  gem.name = "yelp"
  gem.homepage = "http://github.com/vispatel/yelp"
  gem.license = "MIT"
  gem.summary = %Q{Small command line tool that interacts with Yelp}
  gem.description = %Q{Can verify a given email address and password can be used to log in. Find the most popular pub in a given city}
  gem.email = "me@vish.eu"
  gem.authors = ["Vishal Patel"]
  gem.files = Dir["{#{Dir["*"].reject{|f| f.match /^(pkg|VERSION|Rakefile|README.textile)$/i}.reject{|f| f.match /\.gemspec$/i}.join(",")}}/**/*"]
  gem.executables = ["yelp"]
  # dependencies defined in Gemfile
end
Jeweler::RubygemsDotOrgTasks.new

require 'rspec/core'
require 'rspec/core/rake_task'
RSpec::Core::RakeTask.new(:spec) do |spec|
  spec.pattern = FileList['spec/**/*_spec.rb']
end

RSpec::Core::RakeTask.new(:rcov) do |spec|
  spec.pattern = 'spec/**/*_spec.rb'
  spec.rcov = true
end

require 'cucumber/rake/task'
Cucumber::Rake::Task.new(:features)

task :default => :spec

require 'rake/rdoctask'
Rake::RDocTask.new do |rdoc|
  version = File.exist?('VERSION') ? File.read('VERSION') : ""

  rdoc.rdoc_dir = 'rdoc'
  rdoc.title = "yelp #{version}"
  rdoc.rdoc_files.include('README*')
  rdoc.rdoc_files.include('lib/**/*.rb')
end
