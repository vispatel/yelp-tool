module Yelp
  class Scraper
    require 'mechanize'
    
    def self.check_login(email, password)
      agent = Mechanize.new
      agent.get("https://www.yelp.com/login")
      form = agent.page.forms.first
      form.email = email
      form.password = password
      form.submit
      if agent.page.link_with(:text => /Log Out/)
        "Your account details are valid"
      else
        "Failed to log in to Yelp"
      end
    rescue SocketError
      "Could not connect to the internet"
    end
  end
end