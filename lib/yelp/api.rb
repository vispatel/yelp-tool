module Yelp
  class Api
    require 'oauth'
    require 'json'
    
    AUTH = {
      :consumer_key     => 'jPWkf4RIk5eZ-kSrd-u8jg',
      :consumer_secret  => '5IvktVOrDUICVoRPLgxVB3nScn0',
      :token            => 'uaE4fGBUwv9Y29IsOkaR1GYgGMJQWLGF',
      :token_secret     => '-0u_Y4rMX0BoswKmYcgvu4bHypo',
      :api_host         => 'api.yelp.com'
    }
    
    class << self
      
      def establish_connection 
        consumer = OAuth::Consumer.new(AUTH[:consumer_key], AUTH[:consumer_secret], {:site => "http://#{AUTH[:api_host]}"})
        OAuth::AccessToken.new(consumer, AUTH[:token], AUTH[:token_secret])
      end

      def search(term, location)      
        path = "/v2/search?term=#{term}&location=#{location}&cc=GB&sort=2&limit=1"
        result = JSON.parse(establish_connection.get(path).body)
      
        display_result(result['businesses'].first) if result['businesses'].any? 
      rescue SocketError
        puts "Could not connect to the internet"
      end
    
      def display_result(business)
        puts "The highest rated pub in #{business['location']['city']} is:"
        puts
        puts '='*80
        puts "#{business['name']} - #{business['location']['city']}"
        puts '='*80
        puts
        puts business['location']['display_address'].join(', ')
        puts business['display_phone']
        puts
        puts parse_ratings_url(business['rating_img_url']).to_s + " stars" 
        puts
        puts '='*80
      end

      def parse_ratings_url(img_url)
        match = img_url.match /stars_(\d)[_]?(half)?\.png$/
        "%.1f" % (match[2] ? match[1].to_i + 0.5 : match[1].to_i)
      end
      
    end
    
  end
end