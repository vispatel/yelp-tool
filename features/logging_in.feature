Scenario: Logging in to Yelp with email addresses and passwords

  Feature: Successfully logging in with a valid email and password
    When I run `yelp login <email_address> <password>`
    Then the output should contain "Your account details are valid"

  Feature: Failing to log in to Yelp due to an invalid email or password
    When I run `yelp login <email_address> <password>`
    Then the output should contain "Failed to log in to Yelp"

  Feature: Failing to log in to Yelp due to a connection error
    Given the internet is down
    When I run `yelp login <email_address> <password>`
    Then the output should contain "Could not connect to the internet"