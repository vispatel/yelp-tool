Scenario: Finding popular pubs

  Feature: Find the most popular pub in <City>
    When I run `yelp pub <City>`
    Then the output should contain:
    """
      The highest rated pub in <City> is:

      ================================================================================
      <Name> - <Location>
      ================================================================================

      <Address>
      <Phone>

      <Rating> stars

      ================================================================================
    """

  Examples:
    | City      | Name          | Location  | Address                                 | Phone         | Rating |
    | London    | Holly Bush    | Hampstead | 22 Holly Mount, London, NW3 6SG         | 020 7435 2892 | 4.5    |
    | Cambridge | Free Press    | N/A       | 7 Prospect Row, Cambridge CB1 1DU       | 01223 368337  | 5.0    |
    | Liverpool | The Jacaranda | N/A       | 21 - 23 Slater Street, Liverpool L1 4BW | 0151 708 9424 | 4.5    |

  Feature: Failing to connect due to an connection error
    Given the internet is down
    When I run `yelp pub london`
    Then the output should contain "Could not connect to the internet"
