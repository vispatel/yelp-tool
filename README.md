Yelp Command Line Tool
======================

Markups
-------
 
A small command line tool that interacts with Yelp and provides the following separate bits of functionality.

1. Verify a given email address and password can be used to log in
2. Find the most popular pub in a given city


Installation
------------
Make sure bundler is installed and use it to install missing gems

    gem install bundler
    bundle install

To install the gem use

    rake install

Usage
-----
To check if your username and password are valid use the login command.

    yelp login email@address.com password

To find the most popular pub in London

    yelp pub london

Dependencies
------------
    rake (0.9.2) 
    builder (3.0.0) 
    bundler (1.0.17) 
    diff-lcs (1.1.2) 
    json (1.5.3) with native extensions 
    gherkin (2.4.1) with native extensions 
    term-ansicolor (1.0.5) 
    cucumber (1.0.0) 
    git (1.2.5) 
    jeweler (1.6.4) 
    net-http-digest_auth (1.1.1) 
    net-http-persistent (1.8.1) 
    nokogiri (1.4.6) with native extensions 
    webrobots (0.0.10) 
    mechanize (2.0.1) 
    rcov (0.9.9) with native extensions 
    rspec-core (2.3.1) 
    rspec-expectations (2.3.0) 
    rspec-mocks (2.3.0) 
    rspec (2.3.0)
    thor (0.14.6)
    oauth (0.4.5)

Copyright
---------
Copyright (c) 2011 Vishal Patel. See LICENSE.txt for
further details.
